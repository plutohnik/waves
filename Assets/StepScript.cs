﻿using UnityEngine;
using System.Collections;

public class StepScript : MonoBehaviour {

	public Person person;

	public void StepSound(){
		person.StepSound ();
	}

	public void WaveOff(){
		person.WaveOff ();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
