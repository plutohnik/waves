﻿using UnityEngine;
using System.Collections;

public class Person : MonoBehaviour {

	public GameObject[] eyes;
	public GameObject head;
	public GameObject beggar;
	public AudioSource[] stepSounds;
	public Animator animator;

	private GameObject _target;
	private float _speed = 0.15f;
	private float _checkRate = 1;
	private float _minimumCheckRate = 0.25f;

	private float _awkwardness;
	private bool _active;
	private float _currentCheckTime;
	private Vector3 _lookDirection;
	private Vector3 _headLookDirection;

	public GameObject Target{
		set{
			_target = value;
		}
	}

	public void StepSound(){
		int rand = Random.Range (0, stepSounds.Length);
		stepSounds [rand].Play ();
	}

	public void WaveOff(){
		Debug.Log ("wave off");
		animator.SetBool ("wave", false);
		_headLookDirection = _target.transform.position;
		_speed = 0.15f;
	}

	// Use this for initialization
	void Start () {
		_awkwardness = Random.Range (0f, 1f);
		_lookDirection = _target.transform.position;
		_headLookDirection = _target.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		Movement ();
		UpdateLook ();
	}

	private void UpdateLook(){
		if (_currentCheckTime <= 0) {
			_currentCheckTime = _minimumCheckRate + (_checkRate * (1 - _awkwardness));
			ChooseTargetToLookAt ();
		}
		_currentCheckTime -= Time.deltaTime;
		Vector3 targetDir;
		Vector3 newDir;
		for (int i = 0; i < eyes.Length; i++) {
			targetDir = _lookDirection - eyes[0].transform.position;
			newDir = Vector3.RotateTowards(eyes[0].transform.forward, targetDir, 0.1f, 0.0F);
			eyes [i].transform.rotation = Quaternion.LookRotation(newDir);
		}
		targetDir = _headLookDirection - head.transform.position;
		newDir = Vector3.RotateTowards(head.transform.forward, targetDir, 0.01f, 0.0F);
		head.transform.rotation = Quaternion.LookRotation(newDir);
	}

	private void ChooseTargetToLookAt(){
		if (!animator.GetBool ("wave")) {
			if (_awkwardness < 0.33f || beggar.GetComponent<Player> ().Waving () || _active) {
				float target = Random.Range (0f, 1f);
				if (target >= 0.75f && target < 0.99f) {
					float lookTarget = Random.Range (0f, 1f);
					if (lookTarget >= 0.5f) {
						_lookDirection = beggar.GetComponent<Player> ().head.transform.position;
					} else {
						_lookDirection = beggar.GetComponent<Player> ().body.transform.position;
					}
				} else if (target >= 0.99f){
					if (Vector3.Distance (gameObject.transform.position, beggar.transform.position) < 40 && Vector3.Distance (gameObject.transform.position, _target.transform.position) > 110) {
						_lookDirection = beggar.GetComponent<Player> ().head.transform.position;
						animator.SetBool ("wave", true);
						_headLookDirection = beggar.GetComponent<Player> ().head.transform.position;
						_speed = 0.1f;
					}else {
						_lookDirection = _target.transform.position;
					}
				}else {
					_lookDirection = _target.transform.position;
				}
			}
			else {
				_lookDirection = _target.transform.position;
			}
		}
	}

	private void Movement(){
		if (_target != null) {
			Vector3 temp = Vector3.MoveTowards (gameObject.transform.position, _target.transform.position, _speed);
			transform.position = new Vector3 (temp.x, transform.position.y, transform.position.z);

			if (Mathf.Abs(_target.transform.position.x - transform.position.x) < 1) {
				Destroy (gameObject);
			}
		}
	}

	public void LookedAt(){
		_active = true;
	}
}
