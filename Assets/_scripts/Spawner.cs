﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public GameObject spawnObject;
	public GameObject spawnObjectTarget;
	public GameObject beggar;

	public Vector3 rot;
	public bool randomSpawn = false;
	public float randomSpawnMin;
	public float randomSpawnMax;

	public float spawnTime;

	private float _nextSpawn;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (_nextSpawn <= 0) {
			if (randomSpawn) {
				_nextSpawn = Random.Range (randomSpawnMin, randomSpawnMax);
			} else {
				_nextSpawn = spawnTime;
			}
			Vector3 rndPosWithin = new Vector3(transform.position.x, 0, transform.position.z + Random.Range(-GetComponent<BoxCollider>().bounds.extents.z, GetComponent<BoxCollider>().bounds.extents.z));
			GameObject temp = Instantiate(spawnObject, rndPosWithin, Quaternion.Euler(rot)) as GameObject; 
			temp.GetComponent<Person> ().Target = spawnObjectTarget;
			temp.GetComponent<Person> ().beggar = beggar;
		}
		_nextSpawn -= Time.deltaTime;
	}
}
