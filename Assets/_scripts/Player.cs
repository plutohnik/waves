﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public KeyCode leftKey;
	public KeyCode rightKey;
	public KeyCode upKey;
	public KeyCode downKey;

	public GameObject arm;
	public GameObject body;
	public GameObject head;

	public GameObject mouseIndicator;

	public float maxForwardLean;
	public float maxRotation;

	public float leanSpeed;
	public float rotationSpeed;

	private float _forwardLean;
	private float _bodyRotation;

	private float _currentBodyRotation;
	private float _currentForwardLean;
	private bool _waving;

	public bool Waving(){
		if (arm.transform.rotation.x < -0.2) {
			return true;
		}
		return false;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		CheckForKeyRelease ();
		UpdateBodyRotation ();
		UpdateForwardLean ();
		UpdateArmPosition ();

		RaycastHit hit;
		Vector3 fwd = head.transform.TransformDirection(Vector3.forward);
		if (Physics.Raycast (head.transform.position, fwd, out hit)) {
			if(hit.transform.tag == "Person"){
				if (hit.distance < 40) {
					hit.transform.GetComponentInParent<Person> ().LookedAt ();
				}
			}
		}
		Debug.DrawRay(head.transform.position, fwd * 40, Color.green);
	}

	private void UpdateArmPosition(){
		Vector3 newDir;
		Vector3 targetDir;
		if (Input.mousePosition.y > 100) {
			Vector3 mouse = Input.mousePosition;
			mouse.z = 45;
			//new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 5);
			mouseIndicator.transform.position = Camera.main.ScreenToWorldPoint (mouse);
			targetDir = mouseIndicator.transform.position - arm.transform.position;
		} else {
			targetDir = new Vector3(0,-45,0);
		}
		newDir = Vector3.RotateTowards (arm.transform.forward, targetDir, 0.25f, 0.0F);
		arm.transform.rotation = Quaternion.LookRotation (newDir);
	}

	private void UpdateForwardLean(){
		if (Input.GetKeyDown (upKey)) {
			_forwardLean = -leanSpeed;
		} else if (Input.GetKeyDown (downKey)) {
			_forwardLean = leanSpeed;
		}

		if (_forwardLean > 0) {
			if (_currentForwardLean < maxForwardLean) {
				_currentForwardLean += _forwardLean;
				body.transform.rotation = Quaternion.Euler (new Vector3(_currentForwardLean, body.transform.rotation.eulerAngles.y, 0));
			}
		}

		if(_forwardLean < 0){
			if (_currentForwardLean > -maxForwardLean) {
				_currentForwardLean += _forwardLean;
				body.transform.rotation = Quaternion.Euler (new Vector3(_currentForwardLean, body.transform.rotation.eulerAngles.y, 0));
			}
		}
	}

	private void UpdateBodyRotation(){
		if (Input.GetKeyDown (leftKey)) {
			_bodyRotation = -rotationSpeed;
		} else if (Input.GetKeyDown (rightKey)) {
			_bodyRotation = rotationSpeed;
		}

		if (_bodyRotation > 0) {
			if (_currentBodyRotation < maxRotation) {
				_currentBodyRotation += _bodyRotation;
				body.transform.rotation = Quaternion.Euler (new Vector3(body.transform.rotation.eulerAngles.x, _currentBodyRotation, 0));
			}
		}

		if(_bodyRotation < 0){
			if (_currentBodyRotation > -maxRotation) {
				_currentBodyRotation += _bodyRotation;
				body.transform.rotation = Quaternion.Euler (new Vector3(body.transform.rotation.eulerAngles.x, _currentBodyRotation, 0));
			}
		}
	}

	private void CheckForKeyRelease(){
		if (Input.GetKeyUp (leftKey) || Input.GetKeyUp(rightKey)) {
			_bodyRotation = 0;
		}
		if (Input.GetKeyUp (upKey) || Input.GetKeyUp(downKey)) {
			_forwardLean = 0;
		}
	}
}